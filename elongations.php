<?php
   ob_start();
   header("Content-Type: image/png");

   require "computations.php";

   if (array_key_exists("year", $_GET))
   {
      $options = array("options" => array("min_range" => 1990));
      if (filter_var($_GET["year"], FILTER_VALIDATE_INT, $options))
      {
         $yr = $_GET["year"];
      }
      else
      {
         ob_clean();
         #header("HTTP/1.1 400 Bad Request");
         http_response_code(400);
         echo "400 Bad Request - year is not valid";
         exit(1);
      }
   }
   else
   {
      $d = getdate();
      $yr = $d["year"];
   }

   if (array_key_exists("lang", $_GET) && $_GET["lang"] == "fr")
   {
      $month_names = array("janvier", "fevrier", "mars", "avril", "mai",
                           "juin", "juillet", "aout", "septembre", "octobre",
                           "novembre", "decembre");
      $planet_names = array("Mercure", "Venus", "Mars", "Jupiter", "Saturne");
      $title = "Elongation des planetes";
   }
   else
   {
      $month_names = array("january", "february", "march", "april", "may",
                           "june", "july", "august", "september", "october",
                           "november", "december");
      $planet_names = array("Mercury", "Venus", "Mars", "Jupiter", "Saturn");
      $title = "Planet elongations";
   }

   // define the top left corner of plot frame 
   $px = 130;
   $py = 60;
   // image size
   $iw = $px + 720;
   $ih = $py + 720;
   $img = imagecreatetruecolor($iw, $ih);

   // planet colors
   $merc_color = imagecolorallocate($img, 134, 220, 220);
   $venus_color = imagecolorallocate($img, 196, 143, 40);
   $mars_color = imagecolorallocate($img, 200, 32, 32);
   $jup_color = imagecolorallocate($img, 177, 212, 40);
   $sat_color = imagecolorallocate($img, 32, 224, 70);

   // title
   $title_color = imagecolorallocate($img, 180, 180, 180);
   imagestring($img, 4, 10, 5, $title, $title_color); 
   $lx = 205;
   imageline($img, $lx, 12, $lx+40, 12, $merc_color);
   imageline($img, $lx, 13, $lx+40, 13, $merc_color);
   imagestring($img, 4, $lx+50, 5, $planet_names[0], $title_color); 
   $lx += 120;
   imageline($img, $lx, 12, $lx+40, 12, $venus_color);
   imageline($img, $lx, 13, $lx+40, 13, $venus_color);
   imagestring($img, 4, $lx+50, 5, $planet_names[1], $title_color); 
   $lx += 110;
   imageline($img, $lx, 12, $lx+40, 12, $mars_color);
   imageline($img, $lx, 13, $lx+40, 13, $mars_color);
   imagestring($img, 4, $lx+50, 5, $planet_names[2], $title_color); 
   $lx += 110;
   imageline($img, $lx, 12, $lx+40, 12, $jup_color);
   imageline($img, $lx, 13, $lx+40, 13, $jup_color);
   imagestring($img, 4, $lx+50, 5, $planet_names[3], $title_color); 
   $lx += 120;
   imageline($img, $lx, 12, $lx+40, 12, $sat_color);
   imageline($img, $lx, 13, $lx+40, 13, $sat_color);
   imagestring($img, 4, $lx+50, 5, $planet_names[4], $title_color); 
   

   // plot frame
   imageline($img, $px, $py, $iw-1, $py, $title_color);
   imageline($img, $iw-1, $py, $iw-1, $ih-1, $title_color);
   imageline($img, $px, $ih-1, $iw-1, $ih-1, $title_color);
   imageline($img, $px, $py, $px, $ih-1, $title_color);

   // plot grid
   $grid_color = imagecolorallocate($img, 80, 80, 80);
   for($dgrid = 1; $dgrid < 720; $dgrid += 30)
   {
      imageline($img, $px, $py+$dgrid, $iw-1, $py+$dgrid, $grid_color);
      //imageline($img, $px+$dgrid, $py, $px+$dgrid, $ih-1, $grid_color);
   }
   for($dgrid = 1; $dgrid < 720; $dgrid += 60)
   {
      //imageline($img, $px, $py+$dgrid, $iw-1, $py+$dgrid, $grid_color);
      imageline($img, $px+$dgrid, $py, $px+$dgrid, $ih-1, $grid_color);
   }

   // plot month labels
   for($km = 0; $km < 24; $km++)
   {
      imagestring($img, 4, 10, $py+$km*30+8,
                  sprintf("%s %04d", $month_names[$km%12], $yr+$km/12),
                  $title_color); 
   }

   // plot angle scale
   for($ka = -150; $ka <= 150; $ka += 30)
   {
      if ($ka < 0)
      {
         $offs = -18;
      }
      else if ($ka > 0)
      {
         $offs = -8;
      }
      else
      {
         $offs = -4;
      }
      imagestring($img, 4, $px+360+2*$ka +$offs, 40, sprintf("%-03d", $ka), $title_color);
   }

   // central linge, 0 elongation
   imageline($img, $px+360, $py, $px+360, $ih-1, $title_color);

   function plot($im, $x, $y, $clr)
   {
      //imagesetpixel($im, $x-1, $y, $clr);
      imagesetpixel($im, $x, $y, $clr);
      imagesetpixel($im, $x+1, $y, $clr);
   }

   // plot elongations
   $jd = gregoriantojd(1, 1, $yr)-0.5;
   for($k = 1; $k < 720; $k++)
   {
      $sun_pos = sun_position($jd+$k); 
      $merc_elong = elongation(mercury_position($jd+$k), $sun_pos);
      $venus_elong = elongation(venus_position($jd+$k), $sun_pos);
      $mars_elong = elongation(mars_position($jd+$k), $sun_pos);
      $jupiter_elong = elongation(jupiter_position($jd+$k), $sun_pos);
      $saturn_elong = elongation(saturn_position($jd+$k), $sun_pos);

      plot($img, $px+360+2*$merc_elong, $py+$k, $merc_color);
      plot($img, $px+360+2*$venus_elong, $py+$k, $venus_color);
      plot($img, $px+360+2*$mars_elong, $py+$k, $mars_color);
      plot($img, $px+360+2*$jupiter_elong, $py+$k, $jup_color);
      plot($img, $px+360+2*$saturn_elong, $py+$k, $sat_color);
   }

   // output image
   imagepng($img);
   imagedestroy($img);
   ob_end_flush();
?>
