# Planet elongations

An online application that creates a graph showing the elongation of the naked-eye visible planets during two years

## Requirements

- PHP 7.x
- gd

## Usage

Copy the **elongations.php** and **computations.php** files in the directory tree of the web server.
The **elongation.php** script, when called, creates a PNG image of a graph showing the elongation of the five naked-eye visible planets Mercury, Venus, Mars, Jupiter and Saturn during two years, month by month. 

The elongation is the angle subtended by a planet and the Sun, as seen from an observer on the Earth. The east elongations (on the left side of graph) are negative and the west elongations (on the right side) are positive. 

Some query arguments are allowed:
- *year* specifies the starting year. By default, if not specified the current year is used.
- *lang* specifies the language. Currently only "en" (english) or "fr" (french) is allowed. By default "en" is used.

As an example, the following HTML image tag element displays the elongation graph for 2022-2023, in french:
> \<img src="elongations.php?year=2022&lang=fr" alt="planet elongations"\>
