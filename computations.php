<?php
// -----------------------------------------------------------------------------
// File       : computation.php
// Purpose    : low precision astronomical computations
// Author     : A. Viel, Club d'Astronomie Jupiter du Roannais
// Created on : 2021-APR-06
// Modified on: 2021-JUL-04
// Reference  : Peter Duffett-Smith, Practical Astronomy with your Calculator,
//              3rd ed., Cambridge University Press, 1995.
// -----------------------------------------------------------------------------

// -------------------------------------
//
// Mathematical utility functions
//
// -------------------------------------
// Add or substract 360 to keep the argument in the range 0-360
function mod360($x)
{
   return $x - 360*floor($x/360);
}

// Evaluate a polynomial:
// p[0] + p[1]*x + ... + p[p.length-1].x**(p.length-1)
function polynomial($p, $x)
{
   $xa = 1;
   $r = 0;
   for($i = 0; $i < count($p); $i++)
   {
      $r += $p[$i] * $xa;
      $xa *= $x;
   }
   return $r;
}

// Convert a number from sexagesimal to string
function sexa2string($x1, $x2, $x3, $degr)
{
   if ($degr)
   {  // degree, arcminute, arcsecond
      return sprintf("%03d° %02d' %02d''", $x1, $x2, $x3);
   }
   else
   {  // hour, minute, second
      return sprintf("%02dh %02d' %02d''", $x1, $x2, $x3);
   }
}

function dm2string($d, $m)
{
   return sprintf("%+03d° %02d'", $d, $m);
}

// Product of a 3x3-matrix (defined by its columns) by a 3-elements vector
function column_matrix_dot_vector($a1, $a2, $a3, $x)
{
   return array(
         $a1[0]*$x[0] + $a2[0]*$x[1] + $a3[0]*$x[2],
         $a1[1]*$x[0] + $a2[1]*$x[1] + $a3[1]*$x[2],
         $a1[2]*$x[0] + $a2[2]*$x[1] + $a3[2]*$x[2]
      );
}

// Difference between two 3-elements vectors
function vector_minus_vector($a, $b)
{
   return array(
         $a[0] - $b[0],
         $a[1] - $b[1],
         $a[2] - $b[2]
      );
}

// Norm of a 3-elements vector
function norm_vect($a)
{
   return sqrt($a[0]*$a[0] + $a[1]*$a[1] + $a[2]*$a[2]);
}

// Normalize a 3-elements vector
function normalize_vect($a)
{
   $n = norm_vect($a);
   return array($a[0]/$n, $a[1]/$n, $a[2]/$n);
}

// Scalar product
function scalar_product($x, $y)
{
   return $x[0]*$y[0] + $x[1]*$y[1] + $x[2]*$y[2];
}


// -------------------------------------
//
// Time and angle related functions
//
// -------------------------------------
// decimal degree to degree, minute, second
function deg2dms($x)
{
   $sign = 1;
   if ($x < 0)
   {
      $sign = -1;
   }
   $v = abs($x);
   $deg = floor($v);
   $min = floor(60*($v - $deg));
   $sec = floor(60*(60*($v - $deg) - $min));
   return array($sign*$deg, $min, $sec);
}

// number of julian centuries since J2000.0 epoch
function jd2jcent($jd)
{
   return ($jd - 2451545.0) / 36525.0;
}

// -------------------------------------
//
// Coordinate related functions
//
// -------------------------------------
// Convert from rectangular to spherical coordinates
// Return an array (radius, angle in plane, angle above plane)
// all angles expressed in degree
function rect2spher($p)
{
   $r = norm_vect($p);
   return array($r, 
                mod360(rad2deg(atan2($p[1], $p[0]))),
                rad2deg(asin($p[2]/$r)));
}

// Solve the Kepler equation
//
// Arguments:
// ----------
// M  : the mean anomaly, expressed in radian
// ecc: the eccentricity
//
// Returns:
// --------
// the eccentric anomaly, expressed in radian
function solve_kepler_equation($M, $ecc)
{
   $E = $M;
   $delta = $E - $ecc*sin($E) - $M;
   while(abs($delta) > 1e-6)
   {
      $E -= $delta/(1.0-$ecc*cos($E));
      $delta = $E - $ecc*sin($E) - $M;
   }
   return $E;
}

// -------------------------------------
//
// Ephemeris
//
// -------------------------------------

// Compute the orbital position of an object in rectangular coordinates,
// usually the heliocentric ecliptic rectangular coordinates of the planet
// which orbital elements are given as argument, for Julian day number jd
//
// Arguments:
// ----------
// jd        : the Julian day number at which the position is desired
// jd_epoch  : the Julian day number of the epoch
//             for the following orbital elements
// sid_period: the sidereal period, expressed in day
// mean_lon  : the mean longitude at epoch, expressed in degree
// peri_lon  : the longitude of periastron, expressed in degree
// ecc       : the eccentricity
// sma       : the semi major axis, in arbitrary unit
// incli     : the inclination, expressed in degree
// ascn_lon  : the longitude of the ascending node, expressed in degree
//
// Returns:
// --------
// a 3-elements vector, the rectangular coordinates of object,
// expressed in the same unit as sma
function orbital_position($jd, $jd_epoch, $sid_period,
                          $mean_lon, $peri_lon, $ecc, $sma, $incli, $ascn_lon)
{
   // mean anomaly, expressed in radian
   $mean_anom = 2*pi()*($jd-$jd_epoch)/$sid_period
                   + deg2rad($mean_lon - $peri_lon);
   // eccentric anomaly, expressed in radian
   $ecc_anom = solve_kepler_equation($mean_anom, $ecc);
   // heliocentric orbital plane rectangular coordinates,
   // expressed in the same unit as sma
   $p0 = array(
         $sma*(cos($ecc_anom)-$ecc),
         $sma*sqrt(1.0-$ecc*$ecc)*sin($ecc_anom),    
         0.0
      );
   // argument of perihelion, expressed in radian
   $peri_arg = deg2rad($peri_lon - $ascn_lon);
   $cpa = cos($peri_arg);
   $spa = sin($peri_arg);
   // coordinate system change from orbital plane to heliocentric ecliptic
   $p1 = column_matrix_dot_vector(
         array(  $cpa, $spa, 0 ),
         array( -$spa, $cpa, 0 ),
         array(    0,   0, 1 ),
         $p0
      );
   $ci = cos(deg2rad($incli));
   $si = sin(deg2rad($incli));
   $p2 = column_matrix_dot_vector(
         array( 1,   0,  0 ),
         array( 0,  $ci, $si ),
         array( 0, -$si, $ci ),
         $p1
      );
   $ca = cos(deg2rad($ascn_lon));
   $sa = sin(deg2rad($ascn_lon));
   $p3 = column_matrix_dot_vector(
         array(  $ca, $sa, 0 ),
         array( -$sa, $ca, 0 ),
         array(   0,  0, 1 ),
         $p2
      );
   // return the rectangular coordinates as 3-elements vector,
   // expressed in the unit of sma
   return $p3;
}
define("tropical_year", 365.242191);
define("sidereal_year", 365.2564);    
function planet_position($jd, $epoch, $sid_period,
                         $mean_lon, $peri_lon, $ecc, $sma, $incli, $ascn_lon)
{
   // Earth year definitions, expressed in days
   // get Julian day number of epoch (= first day of year)
   $jd_epoch = gregoriantojd(1, 1, $epoch)-0.5;
   // geocentric ecliptic rectangular coordinates
   $gerc = vector_minus_vector(
      orbital_position($jd, $jd_epoch, $sid_period*tropical_year, $mean_lon, $peri_lon, $ecc, $sma, $incli, $ascn_lon),
      orbital_position($jd, $jd_epoch, sidereal_year, 99.403308, 102.768413, 0.016713, 1, 0, 0) // earth
   );
   // return geocentric ecliptic rectangular coordinates
   return $gerc;
   // geocentric ecliptic spherical coordinates
   //$ecl_lat = rad2deg(asin($gerc[2]/norm_vect($gerc)));
   //$ecl_lon = mod360(rad2deg(atan2($gerc[1], $gerc[0])));
   //return array($ecl_lat, $ecl_lon);
}
function sun_position($jd)
{
   $jd_epoch = gregoriantojd(1, 1, 1990)-0.5;
   $p = orbital_position($jd, $jd_epoch, sidereal_year, 99.403308, 102.768413, 0.016713, 1, 0, 0); // earth
   // sun geocentric ecliptic rectangular coordinates
   // are the opposite of earth coordinates
   return array(-$p[0], -$p[1], -$p[2]);
}
function mercury_position($jd)
{
   return planet_position($jd, 1990, 0.240852, 60.750646, 77.299833, 0.205633, 0.387099, 7.004540, 48.212740);
}
function venus_position($jd)
{
   return planet_position($jd, 1990, 0.615211, 88.455855, 131.430236, 0.006778, 0.723332, 3.394535, 76.589820);
}
function mars_position($jd)
{
   return planet_position($jd, 1990, 1.880932, 240.739474, 335.874939, 0.093396, 1.523688, 1.849736, 49.480308);
}
function jupiter_position($jd)
{
   return planet_position($jd, 1990, 11.863075, 90.638185, 14.170747, 0.048482, 5.202561, 1.303613, 100.353142);
}
function saturn_position($jd)
{
   return planet_position($jd, 1990, 29.471362, 287.690033, 92.861407, 0.055581, 9.554747, 2.488980, 113.576139);
}

// Compute the elongation of a planet with respect to the sun
// expressed in degree
function elongation($planet_gerc, $sun_gerc)
{
   // get the elongation, in absolute value
   $elongation = rad2deg(acos(scalar_product(normalize_vect($planet_gerc),
                                             normalize_vect($sun_gerc))));
   // try to get the sign: <0 for east, >0 for west
   $planet_lon = mod360(rad2deg(atan2($planet_gerc[1], $planet_gerc[0])));
   $sun_lon = mod360(rad2deg(atan2($sun_gerc[1], $sun_gerc[0])));
   $elong_lon = mod360($planet_lon - $sun_lon);  // elongation in longitude
   if ($elong_lon <= 180)
      return -$elongation;
   else
      return $elongation;
}

?>
